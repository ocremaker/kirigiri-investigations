Several elements in this repository are under various licenses.

**GPLv3** (See LEGAL/GPLv3.md)

- All the code (HTML, JS and SCSS) in this repository.
- All the dialog text in this repository.

**LGPLv3** (See LEGAL/LGPLv3.md)

- the [enecss](https://github.com/Ad5001/enecss) library (at css/enecss).

**AI Art** (See LEGAL/About-AI-art.md)

- Clock/base.png

**Creative Commons BY-NC-SA v4.0** (See https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)

- All other images in this repository (.png, .jpeg, .xcf...) Copyright (c) ocremaker
- sfx/voice/idontthinkso.ogg

**Creative Commons BY-NC v4.0** (See https://creativecommons.org/licenses/by-nc/4.0/)

- sfx/audience.ogg (from https://freesound.org/people/Robinhood76/sounds/668975/) Copyright (c) Robinhood76
- sfx/failure.ogg (modified from https://freesound.org/people/OptronTeam/sounds/551174/) Copyright (c) OptronTeam

**Creative Commons BY v4.0** (See https://creativecommons.org/licenses/by/4.0/)

- sfx/strike.ogg (modified from https://freesound.org/people/sandyrb/sounds/148075/) Copyright (c) sandyrb

**Creative Commons BY v3.0** (See https://creativecommons.org/licenses/by/3.0/)

- sfx/voice/idontthinkso.ogg (modified from https://freesound.org/people/deleted_user_1390811/sounds/86414/)

**Creative Commons 0** (See https://creativecommons.org/publicdomain/zero/1.0/)

- sfx/treasure.opus (from https://freesound.org/people/FunWithSound/sounds/456965/)
- sfx/start.opus (from https://freesound.org/people/digimistic/sounds/705174/)
- sfx/dialog.opus (from https://freesound.org/people/MATRIXXX_/sounds/345990/)
- sfx/select.ogg (from https://freesound.org/people/Bertsz/sounds/577062/)
- sfx/slam.ogg (from https://freesound.org/people/Previsionary/sounds/593677/)
- sfx/bullet.ogg (from https://freesound.org/people/martian/sounds/182249/)
- sfx/fire.opus (from https://freesound.org/people/Bratish/sounds/500201/)
- sfx/idea.ogg (modified from https://freesound.org/people/Bertrof/sounds/131647/)
- sfx/flashback.ogg (modified from https://freesound.org/people/SoshJam/sounds/582412/)

**All rights reserved**

- sfx/complete.opus (Phase Result from the Danganronpa: Trigger Happy Havoc OST) Copyright (c) Spike Chunksoft.
- sfx/question.opus (SFX from Phoenix Wright: Ace Attorney) Copyright (c) CAPCOM.
- sfx/voice/auchi_objection.ogg,fuhito_objection.ogg,hinata_objection.ogg (From The Great Ace Attorney Chronicles) Copyright (c) CAPCOM.

**Other license**

- fonts/platform_height/* (See fonts/platform_height/information_and_licence.txt)

In the event you find something objectionable in the prior coverage, please [create an issue](https://framagit.org/ocremaker/kirigiri-investigations/-/issues/new) on the repository tracker, or contact me by email (ocremaker (at) tutanota.com) before issuing a DMCA takedown.
