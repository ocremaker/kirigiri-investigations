// 
// Kirigiri: Investigations - 3D Truth Bullet Examinations
// Copyright (C) 2023-2025  ocremaker
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// 

// Drag Drag support

export async function loadDrag() {
    let transformX = 0
    let transformY = 0
    
    function clientX(e) { return e.clientX || e.changedTouches[0].clientX }
    function clientY(e) { return e.clientY || e.changedTouches[0].clientY }
    
    let maxX = 0
    let maxY = 0

    /**
     * Callback for when the window is resized.
     * Computes the maximum offsets to which the scene can be dragged.
     */
    function resetMaxMoveBox() {
        let w = document.querySelector('.scene figure.root').offsetWidth
        let h = document.querySelector('.scene figure.root').offsetHeight
        maxX = window.innerWidth > w ? (window.innerWidth-w)/2 : (w/2-window.innerWidth/3)
        maxY = window.innerHeight > h ? (window.innerHeight-h)/2 : (h/2-window.innerHeight/3)
    }
    window.addEventListener('resize', resetMaxMoveBox)
    resetMaxMoveBox()

    /**
     * Clamps an X position within the screen size.
     * @param {number} transformX
     * @returns {number}
     */
    function clampX(transformX) { return Math.max(-maxX, Math.min(maxX, transformX)) }
    /**
     * Clamps an Y position within the screen size.
     * @param {number} transformY
     * @returns {number}
     */
    function clampY(transformY) { return Math.max(-maxY, Math.min(maxY, transformY)) }

    /**
     * @param {DragEvent} e
     */
    function handleStartDrag(e) {
        let cn = e.target.className.toString() || ""
        if(cn.includes("interactive") || cn.includes("tooltip") || location.hash.includes("dialog") || location.hash.includes("prompt"))
            return
        let targetElement = e.target.hasAttribute('draggable') ? e.target : document.body
        let lastX = clientX(e)
        let lastY = clientY(e)
        
        document.querySelector(".rotation-tip").style.display = "none"
        document.body.classList.add('disable-transform-animation')

        /**
         * @param {DragEvent} e
         */
        function handleDrag(e) {
            let newX = clientX(e)
            let newY = clientY(e)/**
         * @param {DragEvent} e
         */
            transformX = clampX(transformX + newX - lastX)
            transformY = clampY(transformY + newY - lastY)
            lastX = newX;
            lastY = newY;
            
            targetElement.style.setProperty("--translate-x", transformX+"px")
            targetElement.style.setProperty("--translate-y", transformY+"px")
        }

        targetElement.addEventListener('mousemove', handleDrag)
        targetElement.addEventListener('touchmove', handleDrag)
        
        function endDrag() {
            document.body.classList.remove('disable-transform-animation')
            // Remove handlers
            targetElement.removeEventListener('mousemove', handleDrag, false)
            targetElement.removeEventListener('touchmove', handleDrag, false)
            window.removeEventListener('mouseup', endDrag, false)
            window.removeEventListener('touchend', endDrag, false)
            window.removeEventListener('touchcancel', endDrag, false)
        }

        window.addEventListener('mouseup', endDrag, false)
        window.addEventListener('touchend', endDrag, false)
        window.addEventListener('touchcancel', endDrag, false)
    }
    
    document.body.addEventListener('mousedown', handleStartDrag)
    document.body.addEventListener('touchstart', handleStartDrag)
    document.body.classList.add('rotate-enabled')
}

