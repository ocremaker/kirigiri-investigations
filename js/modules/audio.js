// 
// Kirigiri: Investigations - 3D Truth Bullet Examinations
// Copyright (C) 2023-2025  ocremaker
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// 

// Function for loading audio as promise
export function loadAudioAsync(url) {
    return new Promise((resolve, reject) => {
        let audio = new Audio(url)
        audio.addEventListener('canplaythrough', () => resolve(audio))
        audio.addEventListener('error', reject)
    })
}

/**
 * Returns the linearized (0-1) volume based off the audio object's exponential volume value.
 * @param {HTMLAudioElement} audio
 * @return {number}
 */
export function getLinearVolume(audio) {
    return audio.volume ? Math.sqrt(audio.volume) : 0
}

/**
 * Converts the linearized (0-1) volume to an exponential one and sets it to the audio object's volume value.
 * @param {HTMLAudioElement} audio
 * @param {number} volume
 */
export function setLinearVolume(audio, volume) {
    // Clamp volume to [0,1]
    volume = Math.max(0, Math.min(1, volume))
    // Set volume
    audio.volume = Math.pow(volume, 2)
}