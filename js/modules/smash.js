// 
// Kirigiri: Investigations - 3D Truth Bullet Examinations
// Copyright (C) 2023-2025  ocremaker
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// 

// Allow letters to be smashed to be sent in every direction

export async function loadLetterSmashs() {

    /**
     * Queries all elements from selector and outputs an array.
     * @param {string} selector
     * @returns {HTMLElement[]}
     */
    function queryAllToArray(selector) { return Array.from(document.querySelectorAll(selector)); }

    /**
     * Precalculates and prepares where and how the letters should the letters be smashed.
     * @param {HTMLElement} smash
     */
    function prepareSmash(smash) {
        console.log(smash)
        let mark = smash.querySelector('mark')
        let parentRect = mark.getBoundingClientRect()
        let parentXOffset = (window.innerWidth - parentRect.width) - 30
        for(let letter of smash.querySelectorAll('mark > span')) {
            let rect = letter.getBoundingClientRect()
            let pctX = (rect.x - parentRect.x) / parentRect.width
            let transformX = Math.round(pctX * window.innerWidth - parentXOffset)
            let pctY = 1 - Math.abs(pctX - .5)*2
            let y = (Math.random() - .33) * rect.y * 1.5 * Math.sqrt(Math.sqrt(Math.sqrt(pctY)))
            let targetY = Math.round(y)
            letter.style.setProperty("--origin-x", -transformX*.4+"px")
            letter.style.setProperty("--origin-y", "0px")
            letter.style.setProperty("--target-x", transformX+"px")
            letter.style.setProperty("--target-y", targetY+"px")
            letter.style.setProperty("--rotation", Math.round(45*20*Math.random())+"deg")
            letter.style.setProperty("--tilt-y", Math.round((pctX*2-1)*110)+"deg")
            letter.style.setProperty("--tilt-x", Math.round(-y*180/(rect.y))+"deg")
            letter.style.setProperty("--delay", Math.round(Math.abs(pctX-.5)*10)/15+"s")
        }
    }
    
    let smashes = queryAllToArray("#smashs > .smash")
    for(let i = 0; i < smashes.length; i++) {
        prepareSmash(smashes[i])
    }
}
