// 
// Kirigiri: Investigations - 3D Truth Bullet Examinations
// Copyright (C) 2023-2025  ocremaker
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// 

// Shows a summary of all the investigations already done (optional).

export async function loadResults() {
    
    let bullets = document.querySelectorAll('.prompt-pick .bullet')
    
    let dialogs = document.querySelectorAll('[data-dialog-branch]')
    let dialogsTotal = dialogs.length;
    let dialogsViewed = 0;
    
    let cookieLinks = document.querySelectorAll('a[href="#cookie"]')
    let cookieTotal = cookieLinks.length;
    let cookiesFound = 0;
    
    let resultsDisplay = document.querySelector("#investigation-results")
    let displayResults = resultsDisplay != null

    /**
     * Updates the current investigation progress indicator, and shows the complete modal if complete.
     */
    function updateResults() {
        let text = ""
        if(dialogsTotal !== 0)
            text = `${dialogsViewed}/${dialogsTotal}🔎 `;
        if(cookieTotal !== 0)
            text += `${cookiesFound}/${cookieTotal}🍪 `;
        resultsDisplay.innerText = text;
        
        if(dialogsViewed === dialogsTotal && cookiesFound === cookieTotal)
            setTimeout(() => location.hash = "complete", 500) // Finish investigation
    }

    /**
     * Adds a tick to the tooltips associated with the given dialog branch, indicating that it's already been read.
     *
     * @param {string} dialogBranch
     */
    function addTickToTooltip(dialogBranch) {
        let links = document.querySelectorAll(`a[href="#dialog${dialogBranch}-1"]`)
        for(let link of links) {
            let tooltip = link.parentElement.querySelector('.tooltip')
            if(tooltip)
                tooltip.innerText += " ✓"
        }
    }

    /**
     * Event handler for when the last box of a dialog's been passed.
     */
    function finishedReadingDialog() {
        if(this.getAttribute("data-already-read") === null) {
            this.setAttribute("data-already-read", "true")
            let dialogBranch = this.closest('[data-dialog-branch]').getAttribute("data-dialog-branch")
            addTickToTooltip(dialogBranch)
            if(displayResults) {
                dialogsViewed++;
                updateResults()
            }
        }
    }

    /**
     * Event listener for when a link that adds a cookie to the investigation progress is clicked.
     */
    function foundCookie() {
        if(this.getAttribute("data-already-found") === null) {
            this.setAttribute("data-already-found", "true")
            if(displayResults) {
                cookiesFound++;
                updateResults()
            }
        }
    }
    
    for(let link of cookieLinks)
        link.addEventListener("click", foundCookie)
    
    for(let dialog of dialogs) {
        dialog.lastElementChild.querySelector('a').addEventListener("click", finishedReadingDialog)
    }
    
    for(let bullet of bullets) {
        bullet.addEventListener("click", () => {
            bullet.classList.add("pressed")
            setTimeout(() => bullet.classList.remove("pressed"), 2000)
        })
    }
    
    if(displayResults)
        updateResults(); // Show zeros
}
