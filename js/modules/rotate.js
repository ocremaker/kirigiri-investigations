// 
// Kirigiri: Investigations - 3D Truth Bullet Examinations
// Copyright (C) 2023-2025  ocremaker
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// 

// Drag rotation support

export async function loadRotation() {
    let rotationX = 0
    let rotationY = 0
    
    function clientX(e) { return e.clientX || e.changedTouches[0].clientX }
    function clientY(e) { return e.clientY || e.changedTouches[0].clientY }

    /**
     * @param {MouseEvent|TouchEvent} e
     */
    function handleStartRotation(e) {
        let cn = e.target.className.toString() || ""
        if(cn.includes("interactive") || cn.includes("tooltip") || location.hash.includes("dialog") || location.hash.includes("prompt"))
            return

        let lastX = clientX(e);
        let lastY = clientY(e);

        document.querySelector(".rotation-tip").style.display = "none";
        document.body.classList.add('grabbing')

        /**
         * @param {MouseEvent|TouchEvent} e
         */
        function handleRotation(e) {
            let newX = clientX(e)
            let newY = clientY(e)
            rotationX += Math.round((newX - lastX) * 360 / window.innerWidth);
            rotationY -= Math.round((newY - lastY) * 360 / window.innerHeight);
            lastX = newX;
            lastY = newY;
            
            document.body.style.setProperty("--rotation-y", rotationX+"deg")
            document.body.style.setProperty("--rotation-x", rotationY+"deg")
        }

        document.body.addEventListener('mousemove', handleRotation);
        document.body.addEventListener('touchmove', handleRotation);

        /**
         * Clamps a rotation number between [-180 and +180] to ensure that the rotation when clicking a link
         * isn't too wild.
         * @param {number} rotation
         * @returns {number}
         */
        function parseRotation(rotation) {
            if(rotation < -180) rotation = rotation%360 + 360
            if(rotation > 180) rotation = rotation%360 - 360
            return rotation
        }
        
        function endRotation() {
            // Reset to an acceptable degree
            setTimeout(() => {
                document.body.classList.remove('grabbing')
                document.body.classList.add('disable-transform-animation')
                console.log(rotationX, rotationY)
                rotationX = parseRotation(rotationX)
                rotationY = parseRotation(rotationY)
                console.log(rotationX, rotationY)
                document.body.style.setProperty("--rotation-y", rotationX+"deg")
                document.body.style.setProperty("--rotation-x", rotationY+"deg")
                setTimeout(() => document.body.classList.remove('disable-transform-animation'), 100)
            }, 750)
            
            // Remove handlers
            document.body.removeEventListener('mousemove', handleRotation, false);
            document.body.removeEventListener('touchmove', handleRotation, false);
            window.removeEventListener('mouseup', endRotation, false);
            window.removeEventListener('touchend', endRotation, false);
            window.removeEventListener('touchcancel', endRotation, false);
        }

        window.addEventListener('mouseup', endRotation, false);
        window.addEventListener('touchend', endRotation, false);
        window.addEventListener('touchcancel', endRotation, false);
    }
    
    document.body.addEventListener('mousedown', handleStartRotation)
    document.body.addEventListener('touchstart', handleStartRotation)
    document.body.classList.add('rotate-enabled')
}
