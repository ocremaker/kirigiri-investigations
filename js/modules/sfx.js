// 
// Kirigiri: Investigations - 3D Truth Bullet Examinations
// Copyright (C) 2023-2025  ocremaker
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// 

// Plays all SFX on reader click.

import { loadAudioAsync } from './audio.js';

export async function loadSpecialEffects() {
    let selectLinks = document.querySelectorAll('.scene a[href^="#dialog"]')
    let dialogLinks = document.querySelectorAll('[data-dialog-branch] a.dr-dialog-next')
    let cookieLinks = document.querySelectorAll('a[href="#cookie"]')
    let startIDs = Array.from(document.querySelectorAll('.show-start-modal')).map(elem => elem.id)
    let startLinks = startIDs.map(id => document.querySelectorAll('a[href="#'+id+'"]'))
    let prompts = Array.from(document.querySelectorAll('.prompt-pick'))
    let promptLinks = prompts.map(p => document.querySelectorAll('a[href="#'+p.id+'"]'))
    let promptBullets = prompts.map(p => p.querySelectorAll('.bullet'))
    
    // Load chimes
    let completeChime = await loadAudioAsync('../sfx/complete.opus')
    completeChime.volume = 0.5
    
    let dialogChime = await loadAudioAsync('../sfx/dialog.opus')
    dialogChime.volume = 0.75
    
    let dialogChimeQuestion
    if(document.querySelector('.dr-dialog.question') != null || prompts.length > 0) {
        dialogChimeQuestion = await loadAudioAsync('../sfx/question.ogg')
        dialogChimeQuestion.volume = 0.75
    }
    
    let dialogChimeSlam
    if(document.querySelector('.dr-dialog.trembling') != null) {
        dialogChimeSlam = await loadAudioAsync('../sfx/slam.ogg')
        dialogChimeSlam.volume = 0.3
    }
    
    let dialogChimeIdea
    if(document.querySelector('.dr-dialog.idea') != null) {
        dialogChimeIdea = await loadAudioAsync('../sfx/idea.ogg')
        dialogChimeIdea.volume = 0.5
    }
    
    let dialogChimeFlashback
    if(document.querySelector('.dr-dialog.flashback') != null) {
        dialogChimeFlashback = await loadAudioAsync('../sfx/flashback.ogg')
        dialogChimeFlashback.volume = 1
    }
    
    let dialogAudience
    if(document.querySelector('.dr-dialog.empty') != null) {
        dialogAudience = await loadAudioAsync('../sfx/audience.ogg')
        dialogAudience.volume = .5
        dialogAudience.loop = true
    }
    
    let dialogFailure
    if(document.querySelector('.dr-dialog.failure') != null) {
        dialogFailure = await loadAudioAsync('../sfx/failure.ogg')
        dialogFailure.volume = .5
    }
    
    let selectChime = await loadAudioAsync('../sfx/select.ogg')
    selectChime.volume = 0.8
    
    let startChime
    if(startLinks.length > 0) {
        startChime = await loadAudioAsync('../sfx/start.opus')
        startChime.volume = 0.8
    }
    
    let cookieObtainedChime
    if(cookieLinks.length > 0) {
        cookieObtainedChime = await loadAudioAsync('../sfx/treasure.opus')
        cookieObtainedChime.volume = 0.65
    }
    
    let bulletArmFX
    let bulletFireFX
    let bulletStrikeFX
    if(prompts.length > 0) {
        bulletArmFX = await loadAudioAsync('../sfx/bullet.ogg')
        bulletArmFX.volume = 1
        bulletFireFX = await loadAudioAsync('../sfx/fire.opus')
        bulletFireFX.volume = .25
        bulletStrikeFX = await loadAudioAsync('../sfx/anticipate_strike.ogg')
        bulletStrikeFX.volume = 1
    }
    
    // Add event listeners to play them
    for(let link of dialogLinks) {
        let dialog = link.closest('.dr-dialog')
        let nextBox = dialog.nextElementSibling
        if(nextBox?.classList.contains('trembling') || dialog.classList.contains('objection'))
            link.addEventListener("click", () => dialogChimeSlam.play())
        if(nextBox?.classList.contains('question'))
            link.addEventListener("click", () => dialogChimeQuestion.play())
        if(nextBox?.classList.contains('idea'))
            link.addEventListener("click", () => dialogChimeIdea.play())
        if(nextBox?.classList.contains('flashback'))
            link.addEventListener("click", () => dialogChimeFlashback.play())
        if(nextBox?.classList.contains('failure'))
            link.addEventListener("click", () => dialogFailure.play())
        if(nextBox?.classList.contains('empty') && nextBox?.querySelector('.dr-dialog-name')?.innerText === 'The Audience') {
            link.addEventListener("click", () => {dialogAudience.play(); dialogChime.play()})
            nextBox.querySelector('.dr-dialog-next').addEventListener("click", () => dialogAudience.pause())
        } else
            link.addEventListener("click", () => dialogChime.play())
    }
        
    for(let link of selectLinks)
        link.addEventListener("click", () => selectChime.play())
    
    for(let links of startLinks)
        for(let link of links)
            link.addEventListener("click", () => setTimeout(() => startChime.play(), 450))
    
    for(let link of cookieLinks)
        link.addEventListener("click", () => setTimeout(() => cookieObtainedChime.play(), 500))
    
    for(let i = 0; i < prompts.length; i++) {
        for(let link of promptLinks[i]) {
            // Adding click sounds when displaying the dialog
            link.addEventListener("click", () => {
                setTimeout(() => {dialogChimeFlashback.play()}, 1950)
                for(let j = 0; j < promptBullets[i].length; j++)
                    setTimeout(() => bulletArmFX.play(), 2750+j*500)
            })
            // Adding fire sound when pressed
            for(let bullet of promptBullets[i])
                bullet.addEventListener("click", () => {
                    bulletFireFX.play()
                    let href = bullet.href.split('#')[1]
                    let target = document.getElementById(href)
                    if(target.className.includes("show-smash-"))
                        setTimeout(() => bulletStrikeFX.play(), 1300)
                        //setTimeout(() => bulletStrikeFX.play(), 2600)
                })
        }
    }
    
    window.addEventListener('hashchange', (e) => {
        if(e.newURL.split('#')[1] === 'complete')
            setTimeout(() => completeChime.play(), 100)
    })
}
