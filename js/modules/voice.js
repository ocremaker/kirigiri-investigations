// 
// Kirigiri: Investigations - 3D Truth Bullet Examinations
// Copyright (C) 2023-2025  ocremaker
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// 

// Plays reaction voice lines

import { loadAudioAsync } from './audio.js'

export async function loadVoiceLines() {
    let objections = {
        "Kyoko Kirigiri": await loadAudioAsync("../sfx/voice/idontthinkso.ogg"),
        "Sonotori Hinata": await loadAudioAsync("../sfx/voice/hinata_objection.ogg"),
        "Fuhito Kirigiri": await loadAudioAsync("../sfx/voice/fuhito_objection.ogg"),
        "Prosecutor Auchi": await loadAudioAsync("../sfx/voice/auchi_objection.ogg"),
    }
    
    let boxes = document.querySelectorAll('[data-dialog-branch] .dr-dialog.objection')
    
    for(let box of boxes) {
        let dialogId = box.parentElement.getAttribute('data-dialog-branch')
        let boxId = Array.from(box.parentElement.children).indexOf(box)+1
        let boxLink = box.querySelector('.dr-dialog-next')
        let links = document.querySelectorAll('a[href="#dialog'+dialogId+'-'+boxId+'"]')
        let name = box.querySelector('.dr-dialog-name').textContent.trim()
        let objection = objections[name]
        for(let link of links) {
            link.addEventListener('click', () => {
                setTimeout(() => objection.play(), 200)
                let ended = () => {
                    setTimeout(() => boxLink.click(), 200)
                    objection.removeEventListener('ended', ended)
                }
                objection.addEventListener('ended', ended)
            })
        }
    }
    console.log("Voice lines loaded.")
}
