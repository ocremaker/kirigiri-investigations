// 
// Kirigiri: Investigations - 3D Truth Bullet Examinations
// Copyright (C) 2023-2025  ocremaker
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import { getLinearVolume, setLinearVolume } from "./audio"

const YTID_REG = /([\w\d_-]{11})/i

/**
 * Shorthand function to fetch on CORS proxy.
 * @param url
 * @returns {Promise<Response>}
 */
async function proxyFetch(url) {
    return await fetch("https://test-cors.ocremaker.workers.dev/proxy-new/?apiurl=" + encodeURIComponent(url))
}

/**
 * Fetches an Piped instance that is on the latest(ish) version.
 * @returns {Promise<string>}
 */
async function loadInstance() {
    const instancesReq = await proxyFetch("https://piped-instances.kavin.rocks/")
    if(!instancesReq.ok)
        throw new Error(`Couldn't load Piped Instances API: ${instancesReq.status}`)
    const instances = Array.from(await instancesReq.json())
    // Return random instance
    const instance = instances[Math.floor(Math.random() * instances.length)]
    console.log("Picked instance", instance.name )
    return instance.api_url
}

export async function loadMusicAutoplay() {
    // Loading invidious API url.
    const pipedApiURL = await loadInstance()

    window.currentPlayingMusic = null
    window.bufferedMusics = []
    window.musicTitles = []
    window.musicIDs = []
    let fadeMusics = {
        "in": [],
        "out": []
    }

    /**
     * Gets the current volume from the slider from 0 to 1.
     *
     * @returns {number}
     */
    function getCurrentVolume() {
        let volumeSlider = document.querySelector("#volume-slider")
        if(volumeSlider != null)
            return volumeSlider.value / volumeSlider.max
        else
            return 1
    }

    /**
     * Resets the controls of the player and sets the music title.
     * @param {string} newTitle
     * @param {string} newID
     */
    function resetControls(newTitle, newID) {
        newTitle = newTitle.replace("♬", "<span class=\"music-note\">♬</span>")
        document.querySelector("#music-title").querySelector("span").innerHTML = newTitle
        document.querySelector("#music-link-external").href = `${pipedApiURL}/watch?v=${newID}`
    }

    /**
     * Add listener to button to play given audio when pressed.
     * @param {HTMLLinkElement} button
     * @param {HTMLAudioElement} audio
     * @param {string} title
     * @param {string} id - ID of the YouTube link
     */
    function createAudioOnClickListener(button, audio, title, id) {
        button.addEventListener("click", () => {
            console.log("Playing", title)
            if(window.currentPlayingMusic != null && !window.currentPlayingMusic.paused) {
                fadeMusics.out.push(window.currentPlayingMusic)
                window.currentPlayingMusic = audio
                resetControls(title, id)
                setTimeout(() => {
                    setLinearVolume(audio, 0)
                    audio.play()
                    fadeMusics.in.push(audio)
                }, 250)
            } else {
                window.currentPlayingMusic = audio
                resetControls(title, id)
                setLinearVolume(audio, getCurrentVolume())
                audio.play()
            }
        })
    }

    /**
     * Prepares a YouTube video to be played when a specified element is clicked.
     * Only resolves when video is loaded.
     *
     * @param {string} ytURL - URL of the YouTube video.
     * @param {HTMLLinkElement} startingClickElement - Button which when clicked should start the music.
     * @param {string} title - Title of the music
     * @returns Promise<void>
     */
    function prepareVideo(ytURL, startingClickElement, title) {
        return new Promise((resolve) => {
            if(window.musicTitles.includes(title)) {
                resolve()
                return
            }
            let videoID = YTID_REG.exec(ytURL)[0]
            proxyFetch(`${pipedApiURL}/streams/${videoID}`).then(req => {
                if(!req.ok)
                    req.json()
                       .then(j => {
                           console.log("Could not fetch video info: " + j.error)
                           resolve()
                       })
                       .catch(e => {
                           console.log("Could not fetch video info: " + req.status)
                           resolve()
                       })
                else
                    req.json().then(info => {
                        console.log(info)
                        let maxbitrate = Math.max(...info.audioStreams.map(f => f.bitrate))
                        let audioURL = info.audioStreams.find(f => f.bitrate === maxbitrate).url
                        let audio = new Audio(audioURL)
                        console.log("Creating audio for ", title)
                        audio.title = title
                        audio.loop = true
                        audio.addEventListener("error", (e) => {
                            console.log(title, "could not be loaded:", e.error)
                            resolve()
                        })
                        audio.addEventListener("canplay", () => {
                            console.log(title, "loaded.")
                            // Only register audio if it is loaded
                            // Sometimes, video info can be loaded from Piped, but not load when the URL is requested.
                            window.bufferedMusics.push(audio)
                            window.musicTitles.push(title)
                            window.musicIDs.push(videoID)
                            createAudioOnClickListener(startingClickElement, audio, title, videoID)
                            resolve()
                        })
                        audio.addEventListener("play", () => {
                            if(audio === window.currentPlayingMusic)
                                document.querySelector("#music-playpause").classList.add("play")
                        })
                        audio.addEventListener("pause", () => {
                            if(audio === window.currentPlayingMusic)
                                document.querySelector("#music-playpause").classList.remove("play")
                        })
                    })
            }).catch(err => {
                console.log("Error while fetching music:", err)
                resolve()
            })
        })
    }

    // Listen to add dialog information
    /**
     * Event listener for when the hash is changed (allows to preload the musics associated with the dialog).
     * @param {string} hash
     * @param {boolean} wait - Set to true to wait for the audio to be loaded before finishing promise.
     * @returns {Promise<void>}
     */
    async function processHashChange(hash, wait = false) {
        let dialogId = hash.split("#dialog")[1].split("-")[0]
        let dialog = document.querySelector(`[data-dialog-branch="${dialogId}"]:not([data-dialog-processed=true])`)
        if(dialog !== null) {
            let dialogBoxes = Array.from(dialog.querySelectorAll(".dr-dialog"))
            for(let i = 0; i < dialogBoxes.length - 2; i++) {
                let link = dialogBoxes[i + 1].querySelector("a")?.href
                let text = dialogBoxes[i + 1].querySelector(".dr-dialog-text").textContent.trim()
                console.log(link, text)
                // If the next dialog contains a YouTube link...
                if(link !== null && link.startsWith("https://youtu.be/")) {
                    let next = dialogBoxes[i].querySelector(".dr-dialog-next")
                    let videoPromise = prepareVideo(link, next, text.split("\n")[0])
                    if(wait)
                        await videoPromise
                    let plusOne = dialogBoxes[i + 1].querySelector(".dr-dialog-next")
                    next.href = plusOne.href
                    next.addEventListener("click", () => plusOne.click())
                }
                // If the dialog is no music...
                if(text.includes("♬")) {
                    // Fade out current music
                    let next = dialogBoxes[i].querySelector(".dr-dialog-next")
                    switch(text) {
                        case "♬ No music":
                            next.addEventListener("click", () => {
                                if(window.currentPlayingMusic != null)
                                    fadeMusics.out.push(window.currentPlayingMusic)
                            })
                            break
                        case "♬ Stop music":
                            next.addEventListener("click", () => {
                                if(window.currentPlayingMusic != null) {
                                    window.currentPlayingMusic.pause()
                                }
                            })
                            break
                        case "♬ Play music":
                            next.addEventListener("click", () => {
                                if(window.currentPlayingMusic != null) {
                                    window.currentPlayingMusic.play()
                                    setLinearVolume(window.currentPlayingMusic, getCurrentVolume())
                                }
                            })
                            break
                    }
                    let plusOne = dialogBoxes[i + 1].querySelector(".dr-dialog-next")
                    next.href = plusOne.href
                    next.addEventListener("click", () => plusOne.click())
                }
            }
            dialog.setAttribute("data-dialog-processed", "true")
        }
    }


    window.addEventListener("hashchange", (e) => {
        let hash = e.newURL.split("#")[1]
        if(hash.startsWith("dialog") && hash.endsWith("-1"))
            processHashChange("#" + hash)
    })


    // Apply current situation to default hash
    if(location.hash.startsWith("#dialog"))
        await processHashChange(location.hash, true)

    // Loading music player controls
    let musicPlayer = document.getElementById("music-player")
    musicPlayer.querySelector("#music-previous").addEventListener("click", () => {
        if(window.currentPlayingMusic != null) {
            let i = window.bufferedMusics.indexOf(window.currentPlayingMusic)
            if(i > 0) {
                window.bufferedMusics[i].pause()
                window.bufferedMusics[i - 1].play()
                window.currentPlayingMusic = window.bufferedMusics[i - 1]
                resetControls(window.musicTitles[i - 1], window.musicIDs[i - 1])
            }
        }
    })

    musicPlayer.querySelector("#music-next").addEventListener("click", () => {
        if(window.currentPlayingMusic != null) {
            let i = window.bufferedMusics.indexOf(window.currentPlayingMusic)
            if(i < window.bufferedMusics.length - 1) {
                window.bufferedMusics[i].pause()
                window.bufferedMusics[i + 1].play()
                window.currentPlayingMusic = window.bufferedMusics[i + 1]
                resetControls(window.musicTitles[i + 1], window.musicIDs[i - 1])
            }
        }
    })
    let playpause = musicPlayer.querySelector("#music-playpause")
    playpause.addEventListener("click", () => {
        if(window.currentPlayingMusic != null) {
            if(window.currentPlayingMusic.paused) {
                window.currentPlayingMusic.play()
            } else {
                window.currentPlayingMusic.pause()
            }
        }
    })

    // Updating the volume automatically with the slider
    setInterval(() => {
        let m = window.currentPlayingMusic
        if(m != null) {
            if(!fadeMusics.in.includes(m) && !fadeMusics.out.includes(m))
                setLinearVolume(m, getCurrentVolume())
            if(!musicPlayer.classList.contains("playing"))
                musicPlayer.classList.add("playing")
        }
    }, 500)

    // Fade ins
    setInterval(() => {
        let currentVolume = getCurrentVolume()
        for(let i = 0; i < fadeMusics.out.length; i++) {
            let audio = fadeMusics.out[i]
            setLinearVolume(audio, Math.max(0, getLinearVolume(audio) - (0.10 * currentVolume)))
            if(getLinearVolume(audio) < 0.01) {
                audio.pause()
                fadeMusics.out = fadeMusics.out.filter(a => a !== audio)
                i--
            }
        }
        for(let i = 0; i < fadeMusics.in.length; i++) {
            let audio = fadeMusics.in[i]
            setLinearVolume(audio, Math.min(1, getLinearVolume(audio) + (0.10 * currentVolume)))
            if(getLinearVolume(audio) >= currentVolume) {
                fadeMusics.in = fadeMusics.in.filter(a => a !== audio)
                i--
            }
        }
    }, 250)

}
