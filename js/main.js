// 
// Kirigiri: Investigations - 3D Truth Bullet Examinations
// Copyright (C) 2023-2025  ocremaker
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// 

// Main loader for various modules

import { loadDrag } from './modules/drag.js';
import { loadMusicAutoplay } from './modules/music.js';
import { loadResults } from './modules/results.js';
import { loadRotation } from './modules/rotate.js';
import { loadSpecialEffects } from './modules/sfx.js';
import { loadLetterSmashs } from './modules/smash.js';
import { loadVoiceLines } from './modules/voice.js';

const MODULES = {
    'drag': loadDrag,
    'music': loadMusicAutoplay,
    'results': loadResults,
    'rotate': loadRotation,
    'sfx': loadSpecialEffects,
    'smash': loadLetterSmashs,
    'voice': loadVoiceLines
}


let modulesToLoad = 0

let loadingMeter, loadingText, modules

function loadingLog(text) {
    loadingText.innerHTML = text
    console.log(text)
}

/**
 * Callback for when a module is loaded.
 * @param {string} moduleName
 */
function moduleLoaded(moduleName) {
    modulesToLoad--
    loadingMeter.value = modules.length - modulesToLoad
    loadingLog(`${moduleName} loaded. ${modulesToLoad} remaining.`)
    if(modulesToLoad === 0)
        document.body.classList.remove('loading')
}

window.addEventListener('load', () => {
    let modulesInfo = document.querySelector('meta[name="ki:modules"]');
    document.body.classList.add('loading')
    if(modulesInfo == null)
        throw new Error('Cannot load javascript modules. No meta[name="ki:modules"] found.')
    loadingMeter = document.querySelector('#loading-status > meter')
    loadingText = document.querySelector('#loading-status > p')
    modules = modulesInfo.content.split(',')
    modulesToLoad = modules.length
    loadingMeter.max = modules.length
    for(let module of modules) {
        loadingLog('Loading ' + module + '...')
        MODULES[module]().then(() => moduleLoaded(module))
    }
})
