#!/usr/bin/env sh
#
# Kirigiri: Investigations - 3D Truth Bullet Examinations
# Copyright (C) 2023-2025  ocremaker
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 

# Create public dir
rm -rf public/*
mkdir -p public
# Move to public
cp -r fonts public
cp -r sfx public
cp -r css public
cp -r Clock public
cp -r File public
cp -r IDCard public
cp -r Glasses public
cp -r CrimeScene public
