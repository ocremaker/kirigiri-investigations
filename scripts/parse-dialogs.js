#!/usr/bin/env node
// 
// Kirigiri: Investigations - 3D Truth Bullet Examinations
// Copyright (C) 2023-2025  ocremaker
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

/**
 * @typedef {Object} DialogBox
 * @property {string} by - Speaker of the dialog box.
 * @property {string} text - Contents of the dialog box.
 * @property {string|undefined} when_targeted - List of classes to be applied to the scene when the dialog is being displayed.
 * @property {string|undefined} link - Link to which the next button of the dialog box should redirect to.
 * @property {boolean|undefined} large - True if the text within the box should be enlarged.
 * @property {boolean|undefined} center - True if the text within the box should be centered.
 * @property {string|undefined} type - Type of dialog represented by this box.
 *                                     Can be: question, trembling, objection, idea, flashback, failure
 **/

/**
 * @typedef Smash
 * @property {string} type - Type of smashing animation. Can be: left-right, zoom-center.
 * @property {string} target-x - Offset from the center of the screen at which the bullet should be shot.
 * @property {string} target-y - Offset from the center of the screen at which the bullet should be shot.
 * @property {string} bullet - Name of the bullet smashing the statement.
 *
 * For left-right:
 * @property {string|undefined} from-left - Statement coming from the left.
 * @property {string|undefined} from-right - Statement coming from the right.
 *
 * For zoom-center:
 * @property {string|undefined} single - Content of the statement being zoomed in.
 */

/**
 * @typedef {Object} EvidenceDialogInformation
 * @property {string} target - Path of the file to which the information is targeted.
 * @property {string} lang - (Unused) Language used in the file (for i18n).
 * @property {Object.<string, Array<DialogBox>>} dialogs - List of dialogs that can be shown during the inspection of the evidence.
 * @property {Array<Smash>|undefined} smashs - List of evidences being smashed against dialogs.
 **/

const yaml = require('yaml')
const fs = require('node:fs/promises')
const path = require('node:path')

const DIALOG_COLORS = {
    'Kyoko Kirigiri': 'purple',
    'Makoto Naegi': 'blue',
    'Sonotori Hinata': 'green',
    'Fuhito Kirigiri': 'brown',
    'Prosecutor Auchi': 'maroon',
    'The Judge': 'gray',
    'The Audience': 'empty',
    '': 'empty',
}

const TEMPLATE_DIALOGS = '{{ dialogs }}'
const TEMPLATE_ANCHORS = '{{ dialog_anchors }}'
const TEMPLATE_SMASHS = '{{ smashs }}'
const REG_DIALOGS = new RegExp(' *' + TEMPLATE_DIALOGS, 'g')
const REG_ANCHORS = new RegExp(' *' + TEMPLATE_ANCHORS, 'g')
const REG_SMASHS = new RegExp(' *' + TEMPLATE_SMASHS, 'g')
const REG_MARK = new RegExp('<mark>([^<]+)</mark>', 'g')
const REG_DIALOG_LINK = new RegExp('dialog-href="([^"]+)"', 'g')


/**
 * @param {Array<DialogBox>} dialogData
 * @param {number} dialogId
 * @param {number} indentation
 */
function generateDialog(dialogData, dialogId, indentation) {
    let dialog = [`<div data-dialog-branch="${dialogId}">`]
    let i = 2
    for(let box of dialogData) {
        let link = i > dialogData.length ? '#' : `#dialog${dialogId}-${i++}`
        box.center = box.by === 'The Audience' || box.by === ''
        dialog.push(`    <div class="dr-dialog ${DIALOG_COLORS[box.by]}${box.type ? ' '+box.type : ''}">`)
        if(box.by !== "") {
            dialog.push(`        <div class="dr-dialog-name">${box.by}</div>`)
            dialog.push(`        <div class="dr-speaking">SPEAKING</div>`)
        }
        dialog.push(`        <div class="dr-dialog-box">`)
        dialog.push(`            <${box.center ? 'center' : 'div'} class="dr-dialog-text${box.large ? ' large' : ''}">`)
        dialog.push(`                ${box.text.split('\n').join('<br>\n')}`)
        dialog.push(`            </${box.center ? 'center' : 'div'}>`)
        dialog.push(`            <a class="dr-dialog-next" href="${box.link ? box.link : link}"></a>`)
        dialog.push(`        </div>`)
        dialog.push(`    </div>`)
    }
    dialog.push(`</div>`)
    let whitespace = ' '.repeat(indentation)
    return whitespace+dialog.join('\n'+whitespace)
}

/**
 * @param {Array<DialogBox>} dialogData
 * @param {number} dialogId
 * @param {number} indentation
 */
function generateDialogAnchors(dialogData, dialogId, indentation) {
    let anchors = []
    let i = 1
    for(let box of dialogData) {
        let anchor = `<div id="dialog${dialogId}-${i++}"`
        if(box.when_targeted)
            anchor += ` class="${box.when_targeted}"`
        anchor += "></div>"
        anchors.push(anchor)
    }
    let whitespace = ' '.repeat(indentation)
    return whitespace+anchors.join('\n'+whitespace)
}

/**
 * @param {Smash} smashData
 * @param {number} indentation
 */
function generateSmash(smashData, indentation) {
    let lines = []
    let [t, targetX, targetY] = [smashData.type, smashData['target-x'], smashData['target-y']]
    delete smashData.type; delete smashData['target-x']; delete smashData['target-y'];
    // Construct HTML
    lines.push(`<figure class="smash ${t} position-at-center interactive" style="--target-x: ${targetX}; --target-y: ${targetY}">`)
    for(let [itemType, text] of Object.entries(smashData)) {
        // Separate every letter in a span in mark.
        if(text.match(REG_MARK))
            text = text.replace(REG_MARK, (m, text) => `<mark><span>${text.split('').join('</span><span>').replace(/ /g, '&nbsp;')}</span></mark>`)
            
        // Add to line
        lines.push(`    <figure class="${itemType}">${text}</figure>`)
    }
    lines.push(`</figure>`)
    let whitespace = ' '.repeat(indentation)
    return whitespace+lines.join('\n'+whitespace)
}

/**
 *
 * @param {EvidenceDialogInformation} info
 * @param {string} htmlContents
 * @returns {string} The generated HTML contents
 */
function parseHTMLContent(info, htmlContents) {
    let dialogNamesToId = {}
    let unlinkedDialogs = []
    let dialogs = []
    let anchors = []
    let indent = 8
    let i = 1;
    for(let [name, dialog] of Object.entries(info.dialogs)) {
        dialogs.push(generateDialog(dialog, i, indent))
        anchors.push(generateDialogAnchors(dialog, i, indent))
        dialogNamesToId[name] = i
        unlinkedDialogs.push(name)
        i++
    }
    let newContent = htmlContents.replace(REG_DIALOGS, dialogs.join('\n'))
    newContent = newContent.replace(REG_ANCHORS, anchors.join('\n'))
    newContent = newContent.replace(REG_DIALOG_LINK, (match, dialogName) => {
        let newAttribute = ""
        if(unlinkedDialogs.includes(dialogName)) // Mark dialog as used so no warning needed about it.
            unlinkedDialogs = unlinkedDialogs.filter(n => n !== dialogName)
        if(dialogName in dialogNamesToId) {
            newAttribute = `href="#dialog${dialogNamesToId[dialogName]}-1"`
        } else
            console.warn(`[WARN] No dialog named "${dialogName}" found in ${info.target}.`)
        return newAttribute
    })
    for(let dialogName of unlinkedDialogs)
        console.log(`[WARN] Unused dialog "${dialogName}" in ${info.target}.`)
    if(info.smashs !== undefined) {
        // Replace smashs
        let smashs = []
        indent = 20
        for(let i = 0; i < info.smashs.length; i++)
            smashs.push(generateSmash(info.smashs[i], indent))
        newContent = newContent.replace(REG_SMASHS, smashs.join('\n'))
    }
    return newContent
}

async function parseFile(directory, yamlFile) {
    try {
        let yamlInfo = await fs.readFile(path.join(directory, yamlFile), { encoding: 'utf8' });
        let info = yaml.parse(yamlInfo)
        let htmlContents = await fs.readFile(path.join(directory, info.target), { encoding: 'utf8' });
        let parsedContents = parseHTMLContent(info, htmlContents)
        await fs.writeFile(path.join(directory, info.target), parsedContents)
        console.log(`Exported ${directory}/${yamlFile} to ${directory}/${info.target}`)
    } catch (err) {
        console.log(`Error parsing ${yamlFile}:`)
        console.error(err)
    }
}

async function parseSingleDir(directory) {
    let files = await fs.readdir(directory)
    for(let file of files) {
        if(file.endsWith('.dialogs.yaml')) {
            console.info(`Parsing ${directory}/${file}`)
            parseFile(directory, file)
        }
    }
}

async function parseAll() {
    try {
        let dir = path.resolve('public')
        let dirs = await fs.readdir(dir)
        for(let d of dirs) {
            let fstat = await fs.stat(path.join(dir, d))
            if(fstat.isDirectory()) {
                let subdir = path.join(dir, d)
                parseSingleDir(subdir)
            }
        }
            
    } catch(err) {
        console.error(err);
    }
}

parseAll()
