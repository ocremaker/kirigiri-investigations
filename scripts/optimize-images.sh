#!/usr/bin/env sh
#
# Kirigiri: Investigations - 3D Truth Bullet Examinations
# Copyright (C) 2023-2025  ocremaker
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 

# Remove xcf files
find public/ -maxdepth 2 -type f -name "*.xcf" -delete 
# Convert all images to webps
find public/ -maxdepth 3 -type f -name "*.png" -exec convert {} {}.webp \;
find public/ -maxdepth 3 -type f -name "*.jpg" -exec convert {} {}.webp \;
find public/ -maxdepth 3 -type f -name "*.jpeg" -exec convert {} {}.webp \;
# Replace in HTML files.
find public/ -maxdepth 3 -type f -name "*.html" -exec sed -i "s/\.png/\.png\.webp/g" {} +
find public/ -maxdepth 3 -type f -name "*.html" -exec sed -i "s/\.jpg/\.jpg\.webp/g" {} +
find public/ -maxdepth 3 -type f -name "*.html" -exec sed -i "s/\.jpeg/\.jpeg\.webp/g" {} +
